module Main where

import Control.Monad.IO.Class (liftIO)
import Network.HTTP.Types.Status (ok200)
import Web.Scotty (ActionM, scotty, get, post, status, json, jsonData)

import BattleSnake

main :: IO ()
main = do
  putStrLn "Starting Server"
  scotty 3000 $ do
    get  ""       $ json get_info
    get  "/start"   ok
    get  "/end"     ok
    get  "/move"  $ handle_request  -- Not needed except for easier debugging
    post "/move"  $ handle_request
  where ok = status ok200

-- By default print message based on request, calculates move and prints JSON
handle_request :: ActionM ()
handle_request = do
  req <- jsonData
  liftIO $ putStrLn $ log_request req
  let next = move req
  liftIO $ putStrLn $ "Going to reply with " <> show next
  json $ next
