module Schema.Coord where

import Data.Aeson.Schema (schema)

type Coord = [schema|
  { x: Int
  , y: Int
  }
|]
