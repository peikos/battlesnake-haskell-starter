module Schema.Move where

import Data.Aeson.Schema (schema)

type Move = [schema|
  { move: Text
  , shout: Maybe Text
  }
|]
