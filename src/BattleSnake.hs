module BattleSnake where

import Data.Aeson (FromJSON, Value, encode, decode, object, (.=))
import Data.Aeson.Schema (Object, get)
import Data.Maybe (fromJust)
import Data.Text (Text)

-- JSON Schemas for use with [get| ... |]
import Schema.Coord (Coord)
import Schema.Info (Info)
import Schema.Move (Move)
import Schema.Request (Request)
import Schema.Snake (Snake)

type Coordinate = (Int, Int)

-- Source: https://www.e-learn.cn/topic/3465929
coerceJson :: FromJSON a => Value -> a
coerceJson = fromJust . decode . encode

-- Change this to customise your snek
get_info :: Object Info
get_info = coerceJson $ object [ "apiversion" .= ("1"                  :: Text)
                               , "author"     .= ("Peikos"             :: Text)
                               , "color"      .= ("#FF0090"            :: Text)
                               , "head"       .= ("trans-rights-scarf" :: Text)
                               , "tail"       .= ("mystic-moon"        :: Text)
                               ]

-- Function move is where the magic happens!
-- Use [get| req.*] to retrieve information from request.
-- Docs: https://hackage.haskell.org/package/aeson-schemas
-- Structure: see Request.hs
--   or https://docs.battlesnake.com/references/api/sample-move-request
-- Result should be up, down, left or right (with or without shout).
move :: Object Request -> Object Move
move req = [ up    $ Just "Whee!"
           , left  $ Just "Wow!"
           , down  $ Just "Ooh!"
           , right $ Just "Yoinks!"
           ] !! (turn `mod` 4)
  where turn = [get| req.turn |]

-- up, down, left and right yield JSON for given direction, plus optional shout.
up, down, left, right :: Maybe Text -> Object Move
up    = go "up"
down  = go "down"
left  = go "left"
right = go "right"

-- Retrieve head coordinates from request
head_coordinate :: Object Request -> Coordinate
head_coordinate req = convert_coord [get| req.you.head |]

-- Retrieve body coordinates from request
snake_coordinates :: Object Request -> [Coordinate]
snake_coordinates req = map convert_coord [get| req.you.body |]

-- Retrieve food coordinates from request
food_coordinates :: Object Request -> [Coordinate]
food_coordinates req = map convert_coord [get| req.board.food |]

-- Retrieve hazard coordinates from request
hazard_coordinates :: Object Request -> [Coordinate]
hazard_coordinates req = map convert_coord [get| req.board.hazards |]

-- Retrieve enemy coordinates from request (including your own)
-- This could be expanded to distinguish separate snakes, including head and hp,
--   which is left as an exercise.
enemy_coordinates :: Object Request -> [Coordinate]
enemy_coordinates req = [get| req.board.snakes |] >>= snake_coords 
  where snake_coords :: Object Snake -> [Coordinate]
        snake_coords s = map convert_coord [get| s.body |]

-- Extracting integers from the JSON coordinate object
convert_coord :: Object Coord -> Coordinate
convert_coord coord = ([get| coord.x |], [get| coord.y |])

-- Formatting the JSON
go :: Text -> Maybe Text -> Object Move
go dir shout = coerceJson $ object [ "move" .= dir
                                   , "shout" .= shout
                                   ]

-- Logging based on request, just turn number for now
log_request :: Object Request -> String
log_request req = "Contemplating next move (turn " <> show [get| req.turn |] <> ") ... 🤔"
