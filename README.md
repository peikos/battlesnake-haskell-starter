# BattleSnake

Starter code for BattleSnake in Haskell, developed for the Wednesday Evening Creative Coding sessions in the HU Turing Lab and usable for any student endeavouring to attempt the challenge in Haskell.

## Quick Start

This project uses Stack and should be compiled with `stack build` and run with `stack run`. By default, a server is started on port 3000. This port should be internet-accessible in order to be eligible for participation on [Battlesnake.com](https://battlesnake.com). Please consult [getting started](https://docs.battlesnake.com/guides/getting-started), the [API documentation](https://docs.battlesnake.com/references/api) and [this example](https://docs.battlesnake.com/references/api/sample-move-request) for reference on the structure of the JSON provided to your snake. The JSON is handled using the [Aeson-Schemas](https://hackage.haskell.org/package/aeson-schemas) library.

The entry point for snake behaviour customisation is the `move :: Object Request -> Object Move` function. The `Object Request` can be queried using Aeson-schema's `[get| ... |]` functionality to get information on the board size and snake / food positions. The result should be a short JSON snippet providing a direction and (optionally) a shout, which can be created using `left :: Maybe Text -> Object Move` and corresponding functions for the other cardinal directions. The optional `Maybe Text` argument can be used to add a shout.

Happy hacking!

## Future Work
The current version is stateless, and allows the computation of the next move based on the current game state. The information provided in the game state is quite extensive and should be enough for most strategies without requiring access to previous moves or gamestates. For more elaborate AIs, for example predicting opponents moves based on their track record, a stateful version might be included at some later point.
